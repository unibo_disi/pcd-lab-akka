plugins {
    scala
}

repositories {
    jcenter()
}

val scalaMajor = "2.12"
val scalaMinor = "8"
val scalaVersion = "$scalaMajor.$scalaMinor"
val akkaVersion = "2.5.22"

dependencies {
    implementation("org.scala-lang:scala-library:$scalaVersion")

    implementation("com.typesafe.akka:akka-actor_$scalaMajor:$akkaVersion")
    implementation("com.typesafe.akka:akka-actor-typed_$scalaMajor:$akkaVersion")

    implementation("com.typesafe.akka:akka-remote_$scalaMajor:$akkaVersion")
    implementation("com.typesafe.akka:akka-cluster_$scalaMajor:$akkaVersion")
    implementation("com.typesafe.akka:akka-cluster-tools_$scalaMajor:$akkaVersion") // pub/sub etc.
    runtime("com.typesafe.akka:akka-cluster-typed_$scalaMajor:$akkaVersion")


    testImplementation("junit:junit:4.12")
    testImplementation("org.scalatest:scalatest_$scalaMajor:3.0.5")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<ScalaCompile>().configureEach {
    options.apply {
        targetCompatibility = JavaVersion.VERSION_1_8.toString()
        sourceCompatibility = JavaVersion.VERSION_1_8.toString()
    }
}