package pcd.actors.ex0_simple

import akka.actor.{Actor, ActorSystem, DeadLetter, Props}
import akka.event.Logging
import com.typesafe.config.ConfigFactory

class PingPongActor extends Actor {
  val log = Logging(context.system, this)

  def workingBehaviour: Receive = {
    case "ping" =>
      log.debug("Got a ping")
      sender ! "pong"
    case "stop" =>
      log.debug("About to stop")
      context.unbecome()
  }

  override def receive: Receive = {
    case "start" =>
      log.debug("About to start")
      context.become(workingBehaviour)
  }

  override def unhandled(msg: Any): Unit =
    log.debug(s"Unhandled $msg")
}

class DeadletterListener extends Actor {
  override def receive: Receive = {
    case d: DeadLetter => println("Deadletter: " + d.message + " by " + d.sender + " aimed at " + d.recipient)
  }
}

/*
 - Try to think at the sequence of log messages before running the system.
 - What is the log message mentioning deadletters? Try to navigate the code to get a hint.
 */
object PingPongApp extends App {
  val system = ActorSystem("pingpong-actor-system", ConfigFactory.parseString("""akka.loglevel = "DEBUG""""))

  system.eventStream.subscribe(system.actorOf(Props(classOf[DeadletterListener])), classOf[DeadLetter])

  val ponger = system.actorOf(Props(classOf[PingPongActor]), "ponger")
  ponger ! "ping"
  ponger ! "start"
  ponger ! "ping"
  ponger ! "stop"
  ponger ! "start"
  ponger ! "stop"
}
