package pcd.actors.ex4_clustering

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.cluster.Cluster
import akka.cluster.ClusterEvent._
import akka.cluster.pubsub.DistributedPubSub
import akka.routing.RandomRoutingLogic
import com.typesafe.config.ConfigFactory
import pcd.actors.ex1_prodcons._

import scala.concurrent.duration._


/**
  * On clustering
  * - Notice cluster events as logged by [SimpleClusterListener]
  *
  * On pub/sub:
  * - A PubSubMediator must be started in any node of the cluster; this is done by loading a corresponding extension (see clustering.conf)
  **/
object ProducersConsumersClusteringApp extends App {
  val system = ActorSystem("MySystem", ConfigFactory.parseString("""akka.remote.netty.tcp.port = 9005""")
    .withFallback(ConfigFactory.load("clustering")))
  val system2 = ActorSystem("MySystem", ConfigFactory.parseString("""akka.remote.netty.tcp.port = 9007""")
    .withFallback(ConfigFactory.load("clustering")))

  system.actorOf(Props(classOf[SimpleClusterListener]))

  // Pub/sub example: publisher and subscriber on two different nodes
  val pub = system.actorOf(Props(classOf[PublisherActor]), "publisher")
  val sub = system2.actorOf(Props(classOf[SubscriberActor]), "subscriber")

  // System instantiation
  val loadBalancer = system2.actorOf(LoadBalancer.props(RandomRoutingLogic()), "LoadBalancer")
  val scheduler = system.actorOf(Scheduler.props(50 millis), "Scheduler")
  val producers = (1 to 10) map { i => system.actorOf(Producer.props(() => (Math.random() * 100).toInt, loadBalancer), s"Producer$i") }
  val consumers = (1 to 50) map { i => system2.actorOf(Consumer.props((v: Int) => println(v)), s"Consumer$i") }
  producers.foreach(p => scheduler ! AddSchedulable(p))

  consumers.foreach(c => loadBalancer ! AddConsumer(c))

  scheduler ! Start

  Thread.sleep(2000)

  pub ! "hello"
  pub ! "akka"
  pub ! "pub/sub"
}

class SimpleClusterListener extends Actor with ActorLogging {

  val cluster = Cluster(context.system)

  override def preStart(): Unit = {
    cluster.subscribe(self, initialStateMode = InitialStateAsEvents, classOf[MemberEvent], classOf[UnreachableMember])
  }
  override def postStop(): Unit = cluster.unsubscribe(self)

  def receive = {
    case MemberUp(member) =>
      log.debug("Member is Up: {}", member.address)
    case UnreachableMember(member) =>
      log.debug("Member detected as unreachable: {}", member)
    case MemberRemoved(member, previousStatus) =>
      log.debug("Member is Removed: {} after {}", member.address, previousStatus)
    case MemberJoined(m) =>
      log.debug(s"Member has joined: $m")
    case o: MemberEvent =>
      log.debug(s"Member Event: $o")
  }
}

class SubscriberActor extends Actor with ActorLogging {
  import akka.cluster.pubsub.DistributedPubSubMediator.{Subscribe, SubscribeAck}
  val mediator = DistributedPubSub(context.system).mediator
  mediator ! Subscribe(PubSubTopics.someTopic, self) // subscribe to topic "content"

  def receive = {
    case s: String => log.debug("Got {}", s)
    case SubscribeAck(Subscribe(PubSubTopics.`someTopic`, None, `self`)) => log.debug("subscribing")
  }
}

class PublisherActor extends Actor {
  import akka.cluster.pubsub.DistributedPubSubMediator.{Publish}
  val mediator = DistributedPubSub(context.system).mediator
  def receive = {
    case in: String =>
      context.system.log.debug(s"Got ${in} to publish")
      mediator ! Publish(PubSubTopics.someTopic, in.toUpperCase)
  }
}

object PubSubTopics {
  val someTopic = "content"
}