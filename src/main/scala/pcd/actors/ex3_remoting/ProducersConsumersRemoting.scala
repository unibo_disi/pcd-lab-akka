package pcd.actors.ex3_remoting

import akka.actor.{ActorSystem, Address, Deploy}
import akka.remote.RemoteScope
import akka.routing.RandomRoutingLogic
import com.typesafe.config.ConfigFactory
import scala.concurrent.duration._
import pcd.actors.ex1_prodcons._


/**
  * Same as before, but now we have two remote ActorSystems.
  * 1) hosts scheduler and producers
  * 2) hosts load balancer and producers
  *
  * Notice:
  * - Location transparency
  * - Load balancer is deployed remotely by configuration
  * - Consumers are deployed remotely *programmatically* (cannot express deployment paths like "/Consumer*")
  * - Consumers are instantiated remotely on __system2 but they somewhat belong to system
  * - Had to define custom serialization for RandomRoutingLogic (as used in LoadBalancer props), see [pcd.actors.serialization.MyOwnSerializer]
  * - In this case, blocking consumers don't block the producers as they use distinct thread pools
  **/
object ProducersConsumersRemotingApp extends App {
  val __system2 = ActorSystem("MySystem", ConfigFactory.load("remoting2"))
  val system = ActorSystem("MySystem", ConfigFactory.load("remoting1"))

  val loadBalancer = system.actorOf(LoadBalancer.props(RandomRoutingLogic()), "LoadBalancer")
  val scheduler = system.actorOf(Scheduler.props(50 millis), "Scheduler")
  val producers = (1 to 10) map { i => system.actorOf(Producer.props(() => (Math.random() * 100).toInt, loadBalancer), s"Producer$i") }
  val consumers = (1 to 50) map { i => system.actorOf(Consumer.props((v: Int) => println(v)).withDeploy(
    Deploy(scope = RemoteScope(Address("akka.tcp","MySystem","127.0.0.1",9007)))
  ), s"Consumer$i") }
  producers.foreach(p => scheduler ! AddSchedulable(p))
  consumers.foreach(c => loadBalancer ! AddConsumer(c))
  scheduler ! Start

  system.actorSelection(s"akka.tcp://MySystem@127.0.0.1:9005/user/Consumer1").resolveOne(2 seconds).onComplete(
    tried => system.log.debug("Resolve attempt #0: " + tried))(system.dispatcher)
  system.actorSelection(s"akka.tcp://MySystem@127.0.0.1:9005/*/Consumer1").resolveOne(2 seconds).onComplete(
    tried => system.log.debug("Resolve attempt #1: " + tried))(system.dispatcher)
  system.actorSelection(s"akka.tcp://MySystem@127.0.0.1:9007/user/Consumer1").resolveOne(2 seconds).onComplete(
    tried => system.log.debug("Resolve attempt #2: " + tried))(system.dispatcher)
  system.actorSelection(s"akka.tcp://MySystem@127.0.0.1:9007/remote/akka.tcp/MySystem@127.0.0.1:9005/user/Consumer1").resolveOne(2 seconds).onComplete(
    tried => system.log.debug("Resolve attempt #3: " + tried))(system.dispatcher)
  system.actorSelection(s"akka.tcp://MySystem@127.0.0.1:9007/remote/akka.tcp/MySystem@127.0.0.1:9005/*/Consumer1").resolveOne(2 seconds).onComplete(
    tried => system.log.debug("Resolve attempt #4: " + tried))(system.dispatcher)
}